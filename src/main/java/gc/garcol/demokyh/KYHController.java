package gc.garcol.demokyh;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/hook")
@RequiredArgsConstructor
public class KYHController {

    @Data
    @NoArgsConstructor
    public static class KYHResponse {
        private Boolean success;
        private Map<String, Object> data;
    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class KYHRequest {
        private String eventType;
        private List<EventData> eventData;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor(staticName = "of")
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class EventData {
        private Integer tokenId;
        private Boolean isListing;
    }

    @GetMapping
    public KYHResponse testCallHook() {
        KYHRequest req = new KYHRequest();
        req.setEventType("mk.listing.v1");
        req.setEventData(Arrays.asList(EventData.of(123, true)));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("x-api-key", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiS05PVyBZT1VSIEhPUlNFIEJFIFdFQiBIT09LIERFViJ9.lGDQFUCP6fJWcM1cQbHtrkcmrB-yBlckLp8WVs3DyLk");

        String url = "https://dev-kyh-api.head-2-head.net/api/update-data/webhook";
        HttpEntity<KYHRequest> httpRequest = new HttpEntity<>(req, headers);
        return new RestTemplate().exchange(url, HttpMethod.PUT, httpRequest, KYHResponse.class).getBody();
    }


}
