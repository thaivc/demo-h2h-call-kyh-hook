package gc.garcol.demokyh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoKyhApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoKyhApplication.class, args);
    }

}
